<?php

$method = filter_input(INPUT_SERVER, 'REQUEST_METHOD');
$uri    = filter_input(INPUT_SERVER, 'REQUEST_URI');

$matches = [];
if (preg_match('/\.([a-z\d]{2,5})$/', $uri, $matches)) {
    $contentType = null;
    switch ($matches[1]) {
        case 'css':
            $contentType = 'text/css';
            break;
        case 'js':
            $contentType = 'application/javascript';
            break;
        case 'png':
            $contentType = 'image/png';
            break;
        default:
            echo "unsupported content type";
            exit;
            break;
    }
    header('Content-Type: ' . $contentType);
    require '../angular/dist/frontend/' . $uri;
    exit;
}


/* for testing purposes
header('Access-Control-Allow-Origin: *');
 */

require '../classes/Pooling.php';

$uriParts = explode('?', $uri);
$url      = $uriParts[0];

switch ($url) {
    case '/server/':
        ob_start();
        set_time_limit(10);

        header('Cache-Control: no-cache');
        header('Content-Type: text/event-stream');

        $pooling = new Polling();

        echo str_repeat(' ', 1024 * 64);
        ob_flush();
        flush();

        $currentUserId = filter_input(INPUT_GET, 'id');

        $pooling->setHandler(function($id, $data) use ($currentUserId) {
            if ($id == $currentUserId) {
                return;
            }
            $data['id'] = $id;
            $data['currentPosition'] = [];
            if (!empty($data['movements'])) {
                $data['currentPosition'] = end($data['movements']);
            }
            $content  = 'event: message' . PHP_EOL;
            $content .= 'data: ' . json_encode($data) . PHP_EOL;
            $content .= PHP_EOL . PHP_EOL;
            echo $content;

            //this is for the buffer achieve the minimum size in order to flush data
            echo str_repeat(' ', 1024 * 64 - strlen($content));
            ob_flush();
            flush();
        });

        $pooling->listen(filter_input(INPUT_GET, 'lastQueryTime', FILTER_VALIDATE_INT));

        $content = "event: message\ndata: {\"end\":true,\"lqt\":" . microtime(true) . "}" . PHP_EOL . PHP_EOL . PHP_EOL;
        echo $content;
        echo str_repeat(' ', 1024 * 64 - strlen($content));
        ob_flush();
        flush();
        ob_end_flush();

        break;
    case '/start/':
        $pooling = new Polling();

        $pooling->push('start', filter_input(INPUT_GET, 'id'), [
            'name'  => filter_input(INPUT_GET, 'name'),
            'color' => filter_input(INPUT_GET, 'color'),
        ]);
        break;
    case '/end/':
        $pooling = new Polling();

        $pooling->push('end', filter_input(INPUT_GET, 'id'));
        break;
    case '/update/':
        $pooling = new Polling();

        $pooling->push('update', filter_input(INPUT_GET, 'id'), [
            'posX' => filter_input(INPUT_GET, 'posX', FILTER_VALIDATE_INT),
            'posY' => filter_input(INPUT_GET, 'posY', FILTER_VALIDATE_INT),
        ]);
        break;
    case '/reset/':
        $pooling = new Polling();
        $pooling->reset();
        break;
    case '/debug/':
        $pooling = new Polling();
        $pooling->debug();
        break;
    case '/test-server/':
        require './test-server.php';
        break;
    case '/test-client/':
        require './test-client.php';
        break;
    case '/':
        require '../angular/dist/frontend/index.html';
        break;
    default:
        header('Location: /');
        break;
}