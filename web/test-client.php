<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>it's just a test</title>
        <base href="/">

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/x-icon" href="favicon.ico">
    </head>
    <body>
        test
        <script type="text/javascript">
            var eventSource = new EventSource('/test-server/');
            eventSource.onopen = function(e) {
              console.log("Соединение открыто");
            };

            eventSource.onerror = function(e) {
              if (this.readyState == EventSource.CONNECTING) {
                console.log("Соединение порвалось, пересоединяемся...");
              } else {
                console.log("Ошибка, состояние: " + this.readyState);
              }
            };

            eventSource.onmessage = function(e) {
              console.log("Пришли данные: " + e.data);
            };
        </script>
    </body>
</html>