<?php

getallheaders();

header('Cache-Control: no-cache'); // recommended to prevent caching of event data.
header("Content-Type: text/event-stream");
header("Connection: keep-alive");

/**
 * Constructs the SSE data format and flushes that data to the client.
 *
 * @param string $id Timestamp/id of this connection.
 * @param string $msg Line of text that should be transmitted.
 */
function sendMsg($msg) {
  echo "event: message" . PHP_EOL;
  echo "data: $msg" . PHP_EOL;
  echo PHP_EOL . PHP_EOL;
echo str_repeat(' ', 1024 * 64);
  ob_flush();
  flush();
}

$serverTime = time();

sendMsg('server time: ' . date("h:i:s", time()));

$i = 0;
while ($i < 5) {
    sendMsg('server time: ' . date("h:i:s", time()));
    $i ++;
    sleep(1);
}