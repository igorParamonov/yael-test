example of apache configuration:

``` markdown
<VirtualHost *:80>
    ServerName yael-test
    DocumentRoot "/var/www/yael-test/web"

    <Directory "/var/www/yael-test/web">
        DirectoryIndex index.php
        Options -Indexes +FollowSymLinks +ExecCGI

        AllowOverride All
        Require all granted

        AddHandler fcgid-script .php
        FCGIWrapper /var/www/cgi-bin/php71.fastcgi
    </Directory>
</VirtualHost>
```