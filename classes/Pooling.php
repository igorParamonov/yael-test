<?php

class Polling
{
    /** @var callback */
    protected $handler;

    protected $sharedMemoryUid;

    public function __construct()
    {
        $this->sharedMemoryUid = ftok(__FILE__, 'e') + 12;
    }

    /**
     * @param string $event
     * @param string $id
     * @param array  $data
     */
    public function push($event, $id, array $data = null)
    {
        $shmid = shmop_open($this->sharedMemoryUid, 'c', 0644, 1024 * 1024 /* it's 1gb */);
        if ($shmid) {
            $storageLength = (integer) shmop_read($shmid, 0, 8);
            $storage = [];
            if ($storageLength) {
                $storage = unserialize(strval(shmop_read($shmid, 8, (int) $storageLength)));
            }
            switch ($event) {
                case 'start':
                    $data['movements'] = [
                        [
                            'time' => microtime(true),
                            'posX' => 350,
                            'posY' => 350,
                        ],
                    ];
                    $storage[$id] = [
                        'time' => microtime(true),
                        'data' => $data,
                    ];
                    break;
                case 'end':
                    $storage[$id]['data']['ended'] = true;
                    $storage[$id]['data']['spent'] = round(microtime(true) - $storage[$id]['time'], 2);
                    $storage[$id]['data']['movements'] = [];
                    break;
                case 'update':
                    $data['time'] = microtime(true);
                    $storage[$id]['data']['movements'][] = $data;
                    break;
            }
            $storage[$id]['_updated'] = microtime(true);
            shmop_write($shmid, (integer) strlen(serialize($storage)), 0);
            shmop_write($shmid, serialize($storage), 8);
            shmop_close($shmid);
        }
    }

    public function reset()
    {
        @$shmid = shmop_open($this->sharedMemoryUid, 'c', 0644, 1024 * 1024 /* it's 1gb */);
        if ($shmid) {
            shmop_delete($shmid);
        }
    }

    /**
     * @return array
     */
    public function debug()
    {
        header('Content-Type: text/html');
        @$shmid = shmop_open($this->sharedMemoryUid, 'a', 0, 0);
        if ($shmid) {
            $storageLength = (integer) shmop_read($shmid, 0, 8);
            $storage = unserialize(strval(shmop_read($shmid, 8, (integer) $storageLength)));
            unset($storage['_updated']);
            asort($storage);
            shmop_close($shmid);
            dump($storage);
            die(sprintf("<br>File: <b>%s</b><br>Line: <b>%s</b>", __FILE__, __LINE__));
        }
    }

    /**
     * @param integer $lastQueryTime
     *
     * @return array
     */
    public function listen($lastQueryTime = null)
    {
        if (!$lastQueryTime) {
            $lastQueryTime = 0;
        }
        $endTime = time() + (integer) ini_get('max_execution_time') - 1;
        @$shmid  = shmop_open($this->sharedMemoryUid, 'a', 0, 0);
        $result  = [];
        if ($shmid) {
            while (time() < $endTime) {
                $storageLength    = (integer) shmop_read($shmid, 0, 8);
                $storage          = unserialize(strval(shmop_read($shmid, 8, (integer) $storageLength)));
                $newUpdatedTime   = 0;
                $somethingUpdated = false;
                foreach ($storage as $id => $data) {
                    if ($data['_updated'] > $lastQueryTime) {
                        foreach ($data['data']['movements'] as $key => $movement) {
                            if ($movement['time'] < $lastQueryTime) {
                                unset($data['data']['movements'][$key]);
                            }
                        }
                        $this->_runHandler($id, $data['data'] + [
                            'movements' => array_values($data['data']['movements']),
                        ]);
                        $somethingUpdated = true;
                        if ($data['_updated'] > $newUpdatedTime) {
                            $newUpdatedTime = $data['_updated'];
                        }
                    }
                }
                if (!$somethingUpdated) {
                    break;
                }
                if ($newUpdatedTime > $lastQueryTime) {
                    $lastQueryTime = $newUpdatedTime;
                }
            }
            shmop_close($shmid);
        }

        return $result;
    }

    /**
     * @param callback $callback
     */
    public function setHandler($callback)
    {
        $this->handler = $callback;
    }

    /**
     * @param string $id
     * @param array  $data
     *
     * @return mixed
     */
    protected function _runHandler($id, array $data = [])
    {
        if (!empty($this->handler)) {
            return call_user_func_array($this->handler, [ $id, $data ]);
        }
    }
}