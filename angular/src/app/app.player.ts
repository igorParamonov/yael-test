export class Player {
    public id: string;
    public name: string;
    public color: string;
    public x = 350;
    public y = 350;
    public current = false;
    public movements = {};
    public start;

    constructor(name) {
        this.id    = '_' + Math.random().toString(36).substr(2, 9);
        this.name  = name;
        this.color = this.getRandomColor();
        this.start = (new Date().getTime()) / 1000;
    }

    public getPoints() {
        var points = [];
        for (var i in this.movements) {
            points.push(this.movements[i].x + ',' + this.movements[i].y);
        }

        return points.join(' ');
    }

    private getRandomColor() {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++){
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
}