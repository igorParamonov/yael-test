import { Player } from './app.player';

export class GamePool {
    public players = {};
    public leaderboard = {};
    public currentPlayer: Player;
    private lastQueryTime = 0;

    constructor() {
        this.initServerListener();
    }

    public getLeaderboard() {
        var result = [];
        for (var playerId in this.leaderboard) {
            result.push({
                name:  this.leaderboard[playerId].player.name,
                color: this.leaderboard[playerId].player.color,
                spent: this.leaderboard[playerId].spent
            });
        }

        result.sort(function(a, b) {
            return a.spent - b.spent;
        });

        return result;
    }

    public initServerListener() {
        var self      = this;
        var sse       = new EventSource(
            '/server/' +
            '?lastQueryTime=' + this.lastQueryTime +
            '&id=' + (this.currentPlayer ? this.currentPlayer.id : 0)
        );
        sse.onmessage = function(event) {
            self.onMessage(self, event);
        };
        sse.onerror   = function(event) {
            self.onError(self, event);
        };
    }

    public setCurrentPalyer(player) {
        this.currentPlayer = player;
    }

    public addPlayer(player) {
        this.players[player.id] = player;
        this.request('start', {
            id:    player.id,
            name:  player.name,
            color: player.color
        });
    }

    public deletePlayer(player) {
        delete this.players[player.id];
        this.request('end', {
            id: player.id
        });
        this.leaderboard[player.id] = {
            player: player,
            spent:  Math.round(((new Date().getTime()) / 1000 - player.start) * 100) / 100
        };
    }

    public onPlayerMove(player) {
        var newMovementKey = Object.keys(player.movements).length;
        player.movements[newMovementKey] = {
            x: player.x,
            y: player.y
        };
        this.request('update', {
            id:   player.id,
            posX: player.x,
            posY: player.y
        });
    }

    private request(method, parameters = {}) {
        var xhr = new XMLHttpRequest();

        parameters = parameters ? parameters : {};

        var params = [];
        for (var key in parameters) {
            params.push(key + '=' + encodeURIComponent(parameters[key]));
        }
        var queryString = params.length > 0 ? ('?' + params.join('&')) : '';
        xhr.open('GET', '/' + method + '/' + queryString, true);
        xhr.send();
    }

    public onMessage(gamePool, event) {
        var eventData = JSON.parse(event.data);

        if (eventData.hasOwnProperty('end')) {
            gamePool.lastQueryTime = eventData.lqt;
            return;
        }

        if (!gamePool.players.hasOwnProperty(eventData.id)) {
            var player = new Player(eventData.name);
            player.id    = eventData.id;
            player.color = eventData.color;
            gamePool.players[player.id] = player;
        }

        var targetPlayer = gamePool.players[eventData.id];

        for (var i in eventData.movements) {
            targetPlayer.movements[i] = {
                x: eventData.movements[i].posX,
                y: eventData.movements[i].posY
            };
        }

        if (eventData.ended) {
            delete gamePool.players[targetPlayer.id];
            gamePool.leaderboard[targetPlayer.id] = {
                player: targetPlayer,
                spent:  eventData.spent
            };
        } else if (!targetPlayer.current && eventData.currentPosition) {
            targetPlayer.x = eventData.currentPosition.posX;
            targetPlayer.y = eventData.currentPosition.posY;
        }
    }

    public onError(gamePool, event) {
        event.target.close();
        setTimeout(function() {
            gamePool.initServerListener();
        }, 1000);
        // no events
        var e = e || event, msg = '';
        switch (e.target.readyState) {
            // if reconnecting
            case e.target.CONNECTING:
                msg = 'Reconnecting...';
                break;
            // if error was fatal
            case e.target.CLOSED:
                msg = 'Connection failed. Will not retry.';
                break;
        }
        console.log('EventSource failed: ' + msg);
    }
}