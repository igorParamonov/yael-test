import { Component } from '@angular/core';
import { GamePool } from './app.game-pool';
import { Player } from './app.player';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less']
})
export class AppComponent {
    public gamePool: GamePool;
    private player: Player;
    private mousePressed: boolean = false;

    private $field;
    private $playersList;
    private $name;
    private $start;
    private $end;
    private $initiator;
    private $finalizer;

    objectKeys = Object.keys;

    ngOnInit() {
        this.$field = document.querySelector('#field');
        this.$name  = document.querySelector('#name');
        this.$start = document.querySelector('#start') as HTMLButtonElement;
        this.$end   = document.querySelector('#end') as HTMLButtonElement;

        this.$playersList = document.querySelector('#players-list');

        this.$initiator = document.querySelector('#initiator');
        this.$finalizer = document.querySelector('#finalizer');
    }

    constructor() {
        this.gamePool = new GamePool();
    }

    onInputFocusIn($event) {
        var $item = $event.target;
        var $label = $item.parentNode.querySelector('label');
        $label.classList.add('active');
    }
    onInputFocusOut($event) {
        var $item = $event.target;
        if ($item.value == '') {
            var $label = $item.parentNode.querySelector('label');
            $label.classList.remove('active');
        }
    }

    onNameInput($event) {
        this.$start.disabled = this.$name.value == '' ? true : false;
        if (this.$name.value == '' && !this.$start.classList.contains('disabled')) {
            this.$start.classList.add('disabled');
        } else if (this.$name.value != '' && this.$start.classList.contains('disabled')) {
            this.$start.classList.remove('disabled');
        }
    }

    startGame($event) {
        this.$initiator.classList.add('disabled');

        this.$playersList.classList.remove('disabled');

        this.$finalizer.classList.remove('disabled');
        this.$end.classList.remove('disabled');
        this.$end.disabled = false;

        this.$field.classList.remove('disabled');

        this.player = new Player(this.$name.value);
        this.player.current = true;
        this.gamePool.addPlayer(this.player);
        this.gamePool.setCurrentPalyer(this.player);
    }

    endGame($event) {
        this.$name.value = '';

        this.$initiator.classList.remove('disabled');

        this.$playersList.classList.add('disabled');

        this.$finalizer.classList.add('disabled');
        this.$end.classList.add('disabled');
        this.$end.disabled = true;

        this.$field.classList.add('disabled');

        this.gamePool.deletePlayer(this.player);
        delete this.player;
    }

    onMouseDown($event) {
        if (!this.player) {
            return;
        }
        this.mousePressed = true;
    }
    onMouseUp($event) {
        if (!this.player) {
            return;
        }
        this.mousePressed = false;
    }
    onMouseMove($event) {
        if (!this.player) {
            return;
        }
        if (!this.mousePressed) {
            return;
        }
        var newX = this.player.x + $event.movementX;
        var newY = this.player.y + $event.movementY;
        if (newX < 0) {
            newX = 0;
        }
        if (newX > 700) {
            newX = 700;
        }
        if (newY < 0) {
            newY = 0;
        }
        if (newY > 700) {
            newY = 700;
        }
        this.player.x = newX;
        this.player.y = newY;

        this.gamePool.onPlayerMove(this.player);
    }
}